##############################################################################
################################# Makefile ###################################

CC:=g++
CFLAGS:=-Wall

SRCFILES:= $(wildcard src/*.cpp)
all:$(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CFLAGS) obj/*.o -o bin/Labyrint -lncurses

obj/%.o:src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I../inc

PHONY:clean
clean:
	rm obj/*

PHONY:run
run:
	bin/./Labyrint
