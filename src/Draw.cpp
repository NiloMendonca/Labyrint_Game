#include <iostream>
#include <ncurses.h>		//Biblioteca responsavel pelo carregamento dos graficos
#include <fstream>		//Biblioteca responsavel pelo carregamente de arquivo externo
#include <unistd.h>	//Biblioteca responsavel pelo usleep
#include <stdlib.h>

#include "../inc/GameObject.hpp"
#include "../inc/Draw.hpp"
#include "../inc/Tempo.hpp"

#define time 300

using namespace std;

Draw::Draw (){}
Draw::~Draw (){}

int Draw::Desenha(int x, int y, int i, int j, int codigo, int numFase, int vida){
	setposicaoX(x);	
	setposicaoY(y);
	seti(i);
	setj(j);

	char temp;
	int tecla;
	bool teste = true;
	ifstream fin;

	initscr();		
	raw();			
	noecho();		
	curs_set(0);		
	keypad(stdscr, true);

	switch(codigo){

		case 1:{
///////////////////////////////////// DESENHA INTRODUCAO ////////////////////////////////////
	try{
			fin.open("bin/Telas/Intro.txt");	//Carrega o arquivo da Intro
	if(!fin)
		throw (10);	
			

			while(!fin.eof()){			//Exibe a intro na tela
				for(j=0;j<=getj();j++){
					for(i=0;i<=geti();i++){
						move(getposicaoX()+j, getposicaoY()+i);
						fin.get(temp);
						if(temp == '\n'){
							printw("\n");
							break;
						}
						printw("%c", temp);		
						usleep(time);
						refresh();				
					}
				}
			}
			getch();
			break;
	}catch(...){
		endwin();
		cerr << "Erro ao carregar introducao!:(" << endl;
		exit(1);
	}

		}

		case 2:{
///////////////////////////////////// DESENHA SELECT NIVEL ////////////////////////////////////		
	try{
			fin.open("bin/Telas/SelectLevel.txt");
			if(!fin)
				throw (10);

	}catch(...){
		endwin();
		cerr << "Erro ao carregar selecao!:(" << endl;
		exit(1);
	}

			while(!fin.eof()){
				for(j=0;j<=getj();j++){
					for(i=0;i<=geti();i++){
						move(getposicaoX()+j, getposicaoY()+i);
						fin.get(temp);
						if(temp == '\n'){
							printw("\n");
							break;
						}
						printw("%c", temp);		
						usleep(time);
						refresh();				
					}
					i=0;
				}
			}	

			while(teste){
				tecla = getch();
				switch (tecla) {
					case 49:
						//Fase 1
						move(9, 63);
						printw("                 ");
						teste = false;
						break;
	
					case 50:
						//Fase 2
						move(9, 63);
						printw("                 ");
						teste = false;
						break;

					case 51:
						//Fase 3
						move(9, 63);
						printw("                 ");
						teste = false;
						break;

					case 27:
						//ESC
						move(9, 63);
						printw("                 ");
						teste = false;
						break;

					case 32:
						//SPACE
						move(9, 63);
						printw("                 ");
						teste = false;
						break;
					default:
						move(9, 63);
						printw("Tecla invalida!");
						break;
					}
				}
			break;
		}
		case 3:{
///////////////////////////////////// DESENHA TUTORIAL ////////////////////////////////////
	try{
			fin.open("bin/Telas/Tutorial.txt");
			if(!fin)
				throw (10);

	}catch(...){
		endwin();
		cerr << "Erro ao carregar o Tutorial!:(" << endl;
		exit(1);
	}

			while(!fin.eof()){
				for(j=0;j<=getj();j++){
					for(i=0;i<=geti();i++){
						move(getposicaoX()+j, getposicaoY()+i);
						fin.get(temp);
						if(temp == '\n'){
							printw("\n");
							break;
						}
						printw("%c", temp);		
						usleep(time);
						refresh();				
					}
				}
			}	

			while(teste){
				tecla = getch();
				switch (tecla) {
					case 27:
						//ESC
						move(9, 63);
						teste = false;
						break;

					case '\n':
						//ENTER
						move(9, 63);
						teste = false;
						break;
				}
			}	
			break;
		}

		case 5:{
///////////////////////////////////// DESENHA NEXT ////////////////////////////////////
	try{			
			fin.open("bin/Telas/Next.txt");
			if(!fin)
				throw (10);

	}catch(...){
		endwin();
		cerr << "Erro ao carregar a selecao para o proximo nivel!:(" << endl;
		exit(1);
	}

			while(!fin.eof()){
				for(j=0;j<=getj();j++){
					for(i=0;i<=geti();i++){
						move(getposicaoX()+j, getposicaoY()+i);
						fin.get(temp);
						if(temp == '\n'){
							printw("\n");
							break;
						}
						printw("%c", temp);		
						usleep(time);
						refresh();				
					}
				}
			}	

			while(teste){
				tecla = getch();
				switch (tecla) {
					case 27:
						//ESC
						move(9, 63);
						teste = false;
						break;

					case '\n':
						//Tutorial
						move(9, 63);
						teste = false;
						break;
					case 32:
						//Tutorial
						move(9, 63);
						teste = false;
						break;
				}
			}	
			break;
		}
		case 6:{
///////////////////////////////////// DESENHA WIN ////////////////////////////////////
	try{
			fin.open("bin/Telas/GameWin.txt");
			if(!fin)
				throw (10);

	}catch(...){
		endwin();
		cerr << "Erro ao carregar a tela de GameWin!:(" << endl;
		exit(1);
	}

			while(!fin.eof()){
				for(j=0;j<=getj();j++){
					for(i=0;i<=geti();i++){
						move(getposicaoX()+j, getposicaoY()+i);
						fin.get(temp);
						if(temp == '\n'){
							printw("\n");
							break;
						}
						printw("%c", temp);		
						usleep(time);
						refresh();				
					}
				}
			}	

			usleep(3000000);
			break;
		}

		case 7:{
///////////////////////////////////// CONTINUA ////////////////////////////////////
	try{
			fin.open("bin/Telas/Continua.txt");
			if(!fin)
				throw (10);

	}catch(...){
		endwin();
		cerr << "Erro ao carregar a tela do tempo!:(" << endl;
		exit(1);
	}

			while(!fin.eof()){
				for(j=0;j<=getj();j++){
					for(i=0;i<=geti();i++){
						move(getposicaoX()+j, getposicaoY()+i);
						fin.get(temp);
						if(temp == '\n'){
							printw("\n");
							break;
						}
						printw("%c", temp);		
						usleep(time);
						refresh();				
					}
				}
			}	

			while(teste){
				tecla = getch();
				switch (tecla) {
					case 27:
						//ESC
						move(9, 63);
						teste = false;
						break;

					case '\n':
						//Inicia
						move(9, 63);
						teste = false;
						break;
				}
			}	
			break;
		}

		case 8:{
///////////////////////////////////// GAME OVER ////////////////////////////////////
	try{
			fin.open("bin/Telas/GameOver.txt");
			if(!fin)
				throw (10);

	}catch(...){
		endwin();
		cerr << "Erro ao carregar a tela de GameOver!:(" << endl;
		exit(1);
	}

			while(!fin.eof()){
				for(j=0;j<=getj();j++){
					for(i=0;i<=geti();i++){
						move(getposicaoX()+j, getposicaoY()+i);
						fin.get(temp);
						if(temp == '\n'){
							printw("\n");
							break;
						}
						printw("%c", temp);		
						usleep(time);
						refresh();				
					}
				}
			}	

			tecla = getch();				
			break;
		}
		
	}

	endwin();
	return tecla;
}
