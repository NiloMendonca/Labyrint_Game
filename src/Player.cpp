#include <iostream>
#include <ncurses.h>		//Biblioteca responsavel pelo carregamento dos graficos
#include <fstream>		//Biblioteca responsavel pelo carregamente de arquivo externo
#include <unistd.h>		//Biblioteca responsavel pelo usleep
#include <stdlib.h>

#include "../inc/GameObject.hpp"
#include "../inc/Player.hpp"
#include "../inc/Tempo.hpp"

#define time 300

using namespace std;

Player::Player (){}
Player::~Player (){}

int Player::Desenha(int x, int y, int i, int j, int numFase, int vida){
	setposicaoX(x);	
	setposicaoY(y);
	seti(i);
	setj(j);

	char temp;
	int tecla;
	char vetor[geti()][getj()];	
	int posicaoX, posicaoY;
	int finalX, finalY;
	int testaMp = 0;
	int bonusX[3], bonusY[3], contaBonus=0;
	double tempoAtual, tempoInicial, restante, tempoBonus;

	ifstream fin;

	initscr();		
	raw();			
	noecho();		
	curs_set(0);		
	keypad(stdscr, true);


	Tempo tempo;
	tempoInicial = tempo.Cronometro();

	try{
		if(numFase == 1){
			fin.open("bin/Mapas/Level1.txt");
			tempoBonus = 45;		
		}
		if(numFase == 2){
			fin.open("bin/Mapas/Level2.txt");
			tempoBonus = 60;		
		}
		if(numFase == 3){
			fin.open("bin/Mapas/Level3.txt");
			tempoBonus = 180;		
		}	
	if(!fin)
		throw (10);
	}catch(...){
		endwin();
		cerr << "Erro ao carregar a fase!:(" << endl;
		exit(1);
	}

	while(1){
		if (testaMp==0){
			while(!fin.eof()){			
				for(j=0; j<=getj(); j++){
					for(i=0; i<=geti(); i++){
						fin.get(temp);			
						vetor[i][j] = temp;
						move(j+getposicaoX(), i+getposicaoY());

						if (vetor[i][j] == '\n'){				
							printw("\n");
							break;
						}

						if (vetor[i][j] =='1'){
							printw("#");
						}

						if (vetor[i][j] == '0'){
							printw(" ");				
						}

						if (vetor[i][j] == '2'){
							printw("@");
							posicaoX = i;
							posicaoY = j;
						}

						if (vetor[i][j] == '3'){
							printw(" ");
							finalX = i;
							finalY = j;
						}

						if (vetor[i][j] == '4'){
							printw("$");
							bonusX[contaBonus] = i;
							bonusY[contaBonus] = j;
							contaBonus++;
						}
						refresh();
						usleep(time);
					}	
				}
			}
		}
		testaMp = 1;
		tecla = getch();

		tempoAtual = tempo.Cronometro();
		if (numFase == 1){
			restante = tempoBonus - (tempoAtual - tempoInicial);
		}
		if (numFase == 2){
			restante = tempoBonus - (tempoAtual - tempoInicial);
		}
		if (numFase == 3){
			restante = tempoBonus - (tempoAtual - tempoInicial);
		}

		move(2, 42);
		printw("     ");
		move(2, 95);
		printw("     ");
		refresh();

		move(1, 41);
		printw("TEMPO", restante);
		move(2, 42);
		printw("%.0lf", restante);
		move(1, 93);
		printw("VIDAS", vida);
		move(2, 95);
		printw("%d", vida);
		refresh();

		for(i=0;i<=3;i++){
			if(posicaoX==bonusX[i] && posicaoY==bonusY[i]){
				bonusX[i] = -1;
				bonusY[i] = -1;
				tempoBonus += 15;
			}
		}

		switch (tecla) {
			case KEY_DOWN:
				if(vetor[posicaoX][posicaoY+1] == '0' || vetor[posicaoX][posicaoY+1] == '3' || vetor[posicaoX][posicaoY+1] == '4'){
					vetor[posicaoX][posicaoY] = '0';
					move(getposicaoX()+posicaoY, getposicaoY()+posicaoX);
					printw(" ");
					posicaoY++;
					vetor[posicaoX][posicaoY] = '2';
					move(getposicaoX()+posicaoY, getposicaoY()+posicaoX);
					printw("@");
				}
				break;

			case KEY_UP:
				if(vetor[posicaoX][posicaoY-1] == '0' || vetor[posicaoX][posicaoY-1] == '3' || vetor[posicaoX][posicaoY-1] == '4'){
					vetor[posicaoX][posicaoY] = '0';
					move(getposicaoX()+posicaoY, getposicaoY()+posicaoX);
					printw(" ");
					posicaoY--;
					vetor[posicaoX][posicaoY] = '2';
					move(getposicaoX()+posicaoY, getposicaoY()+posicaoX);
					printw("@");
				}
				break;

			case KEY_LEFT:
				if(vetor[posicaoX-1][posicaoY] == '0' || vetor[posicaoX-1][posicaoY] == '3' || vetor[posicaoX-1][posicaoY] == '4'){
					vetor[posicaoX][posicaoY] = '0';
					move(getposicaoX()+posicaoY, getposicaoY()+posicaoX);
					printw(" ");
					posicaoX--;
					vetor[posicaoX][posicaoY] = '2';
					move(getposicaoX()+posicaoY, getposicaoY()+posicaoX);
					printw("@");
				}
				break;

			case KEY_RIGHT:
				if(vetor[posicaoX+1][posicaoY] == '0' || vetor[posicaoX+1][posicaoY] == '3' || vetor[posicaoX+1][posicaoY] == '4'){
					vetor[posicaoX][posicaoY] = '0';
					move(getposicaoX()+posicaoY, getposicaoY()+posicaoX);
					printw(" ");
					posicaoX++;
					vetor[posicaoX][posicaoY] = '2';
					move(getposicaoX()+posicaoY, getposicaoY()+posicaoX);
					printw("@");
				}
				break;

			case 27:
				testaMp = 2;
				numFase = tecla;
				break;
		}

		if (restante <= 0){
			numFase += 10;
			break;
		}

		if(testaMp == 2){
			break;
		}

		if(posicaoX == finalX && posicaoY == finalY){
			break;
		}
	}
	usleep(10000);
	tecla = numFase;

	endwin();
	return tecla;
}
