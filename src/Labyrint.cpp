///////////////////////// Bibliotecas ///////////////////////////////
#include "../inc/GameObject.hpp"	//Biblioteca com elementos de posicao do jogo
#include "../inc/Draw.hpp"		//Biblioteca com elementos para o desenho na tela
#include "../inc/Player.hpp"

#include <ncurses.h>		//Biblioteca responsavel pelo carregamento dos graficos

using namespace std;

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Funcao Principal /////////////////////////////////////////////

int main(int argc, char* argv[]){

	initscr();		
	raw();			
	noecho();		
	curs_set(0);		
	keypad(stdscr, true);
	is_term_resized(250, 400);

	int numFase, retorno, retornoFase, codigo;		//Definicao de variaveis
	Draw desenha;							//Define a variavel desenha da classe Draw
	Player desenhaFase;
	
	////////////////////////////////////   LIMPA TELA  ////////////////////////////////////////////////
	clear();
	refresh();


	///////////////////////////////////// INTRODUCAO  ////////////////////////////////////////////////
	INTRO:
	
		desenha.setposicaoX(10);					//Seta os valores da posicao x, posicao y, i e j
		desenha.setposicaoY(40);
		desenha.seti(70);
		desenha.setj(70);
		codigo = 1;
		desenha.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), codigo, 0, 0);		

	/////////////////////////////////// SELECAO DE NIVEL  //////////////////////////////////////////
	SELECAO:

	clear();
	refresh();
	
	int vida=3;
	
		desenha.setposicaoX(10);		
		desenha.setposicaoY(40);
		desenha.seti(70);
		desenha.setj(70);
		codigo = 2;
		retorno = desenha.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), codigo, 0, 0);

	//////////////////////////////////// CODIGO PELAS CONFIGURACOES DAS FASES ///////////////////////////////
	
		if(retorno == 49){
			FASE1:
			clear();
			refresh();
	try{
			desenha.setposicaoX(10);
			desenha.setposicaoY(40);
			desenha.seti(70);
			desenha.setj(70);
			codigo = 4;
			numFase = 1;

			retornoFase = desenhaFase.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), numFase, vida);

	}catch(...){
			printw("Erro ao carregar a fase 1!:(");
			refresh();
		}

			if(retornoFase == 11){
				vida--;
				if(vida <= 0){
					goto GAMEOVER;
				}
				goto TEMPOACABOU;
			}
			if(retornoFase == 27){
				goto FINAL;
			}

		}
	
	

		if(retorno == 50){
			FASE2:
			clear();
			refresh();

	try{
			desenha.setposicaoX(9);
			desenha.setposicaoY(38);
			desenha.seti(100);
			desenha.setj(100);
			codigo = 4;
			numFase = 2;

			retornoFase = desenhaFase.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), numFase, vida);

	}catch(...){
			printw("Erro ao carregar a fase 2!:(");
			refresh();
		}

			if(retornoFase == 12){
				vida--;
				if(vida <= 0){
					goto GAMEOVER;
				}
				goto TEMPOACABOU;
			}
			if(retornoFase == 27){
				goto FINAL;
			}
		}


		if(retorno == 51){
			FASE3:
			clear();
			refresh();
	try{
			desenha.setposicaoX(3);
			desenha.setposicaoY(7);
			desenha.seti(150);
			desenha.setj(150);
			codigo = 4;
			numFase = 3;

			retornoFase = desenhaFase.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), numFase, vida);

	}catch(...){
		printw("Erro ao carregar a fase 3!:(");
		refresh();
	}

			if(retornoFase == 13){
				vida--;
				if(vida <= 0){
					goto GAMEOVER;
				}
				goto TEMPOACABOU;
			}
	
			if(retornoFase == 27){
				goto FINAL;
			}

			clear();
			refresh();
	
			desenha.setposicaoX(10);
			desenha.setposicaoY(40);
			desenha.seti(70);
			desenha.setj(70);		
			codigo = 6;

			desenha.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), codigo, 0, 0);

			goto INTRO;

		}


	/////////////////////////////////////// CHAMA TUTORIAL  //////////////////////////////////////////


		if (retorno == 32){
			retorno = 49;

			clear();
			refresh();

			desenha.setposicaoX(10);
			desenha.setposicaoY(40);
			desenha.seti(70);
			desenha.setj(70);
			codigo = 3;

			retorno = desenha.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), codigo, 0, 0);

			if (retorno == '\n' ){
				goto FASE1;
			}
			if (retorno == 27 ){
				goto FINAL;
			}

		}

		if (retorno == 27){
			return 0;
		}

		if (retornoFase == 0){
			goto FINAL;
		}
	

	//////////////////////////////////  SELECAO PROXIMO NIVEL  //////////////////////////////////////////


		if (retornoFase == 1 || retornoFase == 2 || retornoFase == 3){
			clear();
			refresh();

			desenha.setposicaoX(10);
			desenha.setposicaoY(40);
			desenha.seti(70);
			desenha.setj(70);
			codigo = 5;

			retorno = desenha.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), codigo, 0, 0);

			if (retorno == '\n'){
				if(retornoFase == 1)
					goto FASE2;

				if (retornoFase == 2)
					goto FASE3;

				if (retornoFase == 3)
					goto SELECAO;
			}

			if (retorno == 32){
				goto SELECAO;
			}

			if (retorno == 27){
				goto FINAL;
			}
		}

	//////////////////////////////////  TEMPO ACABOU  //////////////////////////////////////////
	TEMPOACABOU:
	clear();
	refresh();
	
		desenha.setposicaoX(10);
		desenha.setposicaoY(40);
		desenha.seti(70);
		desenha.setj(70);
		codigo = 7;

		retorno = desenha.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), codigo, 0, vida);

		if(retorno == '\n'){
			if (retornoFase ==11){
				goto FASE1;
			}
			if (retornoFase ==12){
				goto FASE2;
			}
			if (retornoFase ==13){
				goto FASE3;
			}
		}
		if(retorno == 27){
			goto FINAL;
		}

	//////////////////////////////////  GAME OVER  //////////////////////////////////////////
	GAMEOVER:
	clear();
	refresh();

		desenha.setposicaoX(10);
		desenha.setposicaoY(40);
		desenha.seti(70);
		desenha.setj(70);
		codigo = 8;

		desenha.Desenha(desenha.getposicaoX(), desenha.getposicaoY(), desenha.geti(), desenha.getj(), codigo, 0, 0);

	vida = 3;
	goto INTRO;

	//////////////////////////////////  FINAL  //////////////////////////////////////////
	FINAL:

	clear();
	refresh();

	endwin();
	return(0);
}
////////////////////////////////////////////////////////////   FIM   /////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

