#include <time.h>
#include <iostream>
#include <ncurses.h>
#include "../inc/Tempo.hpp"
#include "../inc/GameObject.hpp"

using namespace std;

Tempo::Tempo(){}
Tempo::~Tempo(){}

double Tempo::Cronometro(){
	time_t tempo;
	struct tm estrutura = {0};
	double segundos;

	estrutura.tm_hour = 0;   estrutura.tm_min = 0; estrutura.tm_sec = 0;

	time(&tempo);

	segundos = difftime(tempo,mktime(&estrutura));

	return segundos;

}
