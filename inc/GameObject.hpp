/////////////////// Classe pai ////////////////////////
#ifndef Game_h
#define Game_h

using namespace std;

class GameObject{
	private:
		int posicaoX, posicaoY, i, j;
	public:
		GameObject();
		~GameObject();
		void setposicaoX(int posicaoX);
		int getposicaoX();
		void setposicaoY(int posicaoY);
		int getposicaoY();
		void seti(int i);
		int geti();
		void setj(int j);
		int getj();
};

#endif
