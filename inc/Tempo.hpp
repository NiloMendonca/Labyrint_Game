#ifndef Time_h
#define Time_h

#include "GameObject.hpp"

using namespace std;

class Tempo:public GameObject{
	public:
		Tempo();
		~Tempo();

		double Cronometro();

};

#endif
