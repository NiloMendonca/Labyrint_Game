#ifndef Draw_h
#define Draw_h

#include "GameObject.hpp"

using namespace std;

class Draw:public GameObject{
	public:
		Draw();
		~Draw();
		int Desenha(int x, int y, int i, int j, int codigo, int numFase, int vida);


};

#endif
