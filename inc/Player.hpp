#ifndef Player_h
#define Player_h

#include "GameObject.hpp"

using namespace std;

class Player:public GameObject{
	public:
		Player();
		~Player();
		int Desenha(int x, int y, int i, int j, int numFase, int vida);	//Polimorfismo

};
#endif
